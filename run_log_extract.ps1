$foundLines = Select-String -pattern "p2k" -Path log.txt -AllMatches | ForEach-Object {$_.Line}
$lowerLimit=[datetime]::Parse((($foundLines | Select-Object -First 1).Split(' ',3) | Select-Object -Index 0,1) -join " ").AddSeconds(-2)
$upperLimit=[datetime]::Parse((($foundLines | Select-Object -Last 1).Split(' ',3) | Select-Object -Index 0,1) -join " ").AddSeconds(2)

$needToLog=$false

get-content log.txt | ForEach-Object {
try
{

if ($needToLog)
{
$_ #output the current item
}

$dateAsText = (($_ | Select-Object -First 1).Split(' ',3) | Select-Object -Index 0,1) -join " "
$date = [datetime]::Parse($dateAsText)

if (-not $needToLog -and (($lowerLimit -le $date) -and ($date -le $upperLimit)))
{
    $needToLog=$true
} 

elseif ($needToLog -and ($date -gt $upperLimit))
{
    $needToLog=$false
}
}
# catch [MethodInvocationException]
catch [System.Management.Automation.MethodInvocationException]
{
#date is malformed, skip it
}
}