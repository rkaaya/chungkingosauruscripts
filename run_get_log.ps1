param([String]$appName="p2k") 
$currentDate = Get-Date -Format "yyyyMMddHHmm"
$containerName = get-content test.txt -ReadCount 1000 | ForEach-Object { $_ -match $appName } | Select-String -Pattern '^[\w-]+' -AllMatches |ForEach-Object{$_.Matches.Value}

$containerName > "${appName}${currentDate}.txt"


# get-content test.txt -ReadCount 1000 | ForEach-Object { $_ -match "p2k" }

# Select-String -Path test.txt -Pattern '^[\w-]+' -AllMatches |ForEach-Object{$_.Matches.Value} > newfile.txt
# powershell.exe -file itunesForward.ps1 -step 15